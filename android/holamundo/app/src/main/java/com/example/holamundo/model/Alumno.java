package com.example.holamundo.model;

import java.util.Date;

public class Alumno {
    private String nombre;
    private long numeroCuenta;
    private Date fechaNacimieneto;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public long getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(long numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public Date getFechaNacimieneto() {
        return fechaNacimieneto;
    }

    public void setFechaNacimieneto(Date fechaNacimieneto) {
        this.fechaNacimieneto = fechaNacimieneto;
    }
}
